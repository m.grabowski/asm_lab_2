;-----------------------------------          CRC16 calculator        ----------------------------------- 
;-----------------------------------  Michał Grabowski 'mkgrabowski'  ----------------------------------- 
;-----------------------------------    Informatyka IEiT, grupa 7     ----------------------------------- 



;-----------------------------------              Data                ----------------------------------- 
data segment 
 	
 	; parser section
 	buffer	            		db 128 	dup('$')	; parse input to buffer
	bufferlen					db 0				; length of PSP buffer
	flag						db 0				; flag for parsing
	argctr 						db 0,'$'			; number of arguments
	arglen						db 0, '$'			; length of parsed (with NULLs) arguments (working)
	legalchars					db "0123456789abcdef"; list of legal chars for argument2
    ;/parser section

    ; arguments section
    argslength					db 64  dup(0)		; length of each argument
    argoffset					db 64 	dup(0) 		; arguments offset tab
    
    argument1					db 128  dup('$')	; argument number 1
    argument2 					db 128  dup('$') 	; argument number 2
    argument3					db 128  dup('$')    ; argument number 3

    version						db 0
    ;/arguments section

    ; debugging args
	newline 					db 10,13, '$' 
 	debug  						db "*$"
 	tmp							db 0, '$'
 	;/debugging args

 	; file variables
 	tmpfilename 				db 64 dup(0)
 	handle1 					dw ?
 	handle2						dw ?

 	filebuffer					db 4096 dup(?)
 	fbufferlength 				dw 0
 	fbufferposition 			dw 0
 	endofreading				db 0
 	
 	;/file variables
 		
 	; CRC16

	crc16low 					db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 000h, 0c1h, 081h, 040h, 001h, 0c0h, 080h, 041h
								db 001h, 0c0h, 080h, 041h, 000h, 0c1h, 081h, 040h

	crc16high 					db 000h, 0c0h, 0c1h, 001h, 0c3h, 003h, 002h, 0c2h
								db 0c6h, 006h, 007h, 0c7h, 005h, 0c5h, 0c4h, 004h
								db 0cch, 00ch, 00dh, 0cdh, 00fh, 0cfh, 0ceh, 00eh
								db 00ah, 0cah, 0cbh, 00bh, 0c9h, 009h, 008h, 0c8h
								db 0d8h, 018h, 019h, 0d9h, 01bh, 0dbh, 0dah, 01ah
								db 01eh, 0deh, 0dfh, 01fh, 0ddh, 01dh, 01ch, 0dch
								db 014h, 0d4h, 0d5h, 015h, 0d7h, 017h, 016h, 0d6h
								db 0d2h, 012h, 013h, 0d3h, 011h, 0d1h, 0d0h, 010h
								db 0f0h, 030h, 031h, 0f1h, 033h, 0f3h, 0f2h, 032h
								db 036h, 0f6h, 0f7h, 037h, 0f5h, 035h, 034h, 0f4h
								db 03ch, 0fch, 0fdh, 03dh, 0ffh, 03fh, 03eh, 0feh
								db 0fah, 03ah, 03bh, 0fbh, 039h, 0f9h, 0f8h, 038h
								db 028h, 0e8h, 0e9h, 029h, 0ebh, 02bh, 02ah, 0eah
								db 0eeh, 02eh, 02fh, 0efh, 02dh, 0edh, 0ech, 02ch
								db 0e4h, 024h, 025h, 0e5h, 027h, 0e7h, 0e6h, 026h
								db 022h, 0e2h, 0e3h, 023h, 0e1h, 021h, 020h, 0e0h
								db 0a0h, 060h, 061h, 0a1h, 063h, 0a3h, 0a2h, 062h
								db 066h, 0a6h, 0a7h, 067h, 0a5h, 065h, 064h, 0a4h
								db 06ch, 0ach, 0adh, 06dh, 0afh, 06fh, 06eh, 0aeh
								db 0aah, 06ah, 06bh, 0abh, 069h, 0a9h, 0a8h, 068h
								db 078h, 0b8h, 0b9h, 079h, 0bbh, 07bh, 07ah, 0bah
								db 0beh, 07eh, 07fh, 0bfh, 07dh, 0bdh, 0bch, 07ch
								db 0b4h, 074h, 075h, 0b5h, 077h, 0b7h, 0b6h, 076h
								db 072h, 0b2h, 0b3h, 073h, 0b1h, 071h, 070h, 0b0h
								db 050h, 090h, 091h, 051h, 093h, 053h, 052h, 092h
								db 096h, 056h, 057h, 097h, 055h, 095h, 094h, 054h
								db 09ch, 05ch, 05dh, 09dh, 05fh, 09fh, 09eh, 05eh
								db 05ah, 09ah, 09bh, 05bh, 099h, 059h, 058h, 098h
								db 088h, 048h, 049h, 089h, 04bh, 08bh, 08ah, 04ah
								db 04eh, 08eh, 08fh, 04fh, 08dh, 04dh, 04ch, 08ch
								db 044h, 084h, 085h, 045h, 087h, 047h, 046h, 086h
								db 082h, 042h, 043h, 083h, 041h, 081h, 080h, 040h
								
	bytehigh 					db 00h ; high byte of CRC	
	bytelow 					db 00h ; low byte of CRC

	crcbuffer 					db 0, 0, 0, 0, 0
	crcbufferend 				db 0

	message1					db "CRC-16 equal$"
	message2 					db "CRC-16 not equal$"
 	;/CRC16

 	; errors
	error0 						db "Error 0: no arguments$"
	error1 						db "Error 1: first argument is neither an option '-v' nor valid DOS filename of existing file $"
	error2 						db "Error 2: invalid number of arguments - should be 2 (no options) or 3 (with option -v)$"
	error3 						db "Error 3: option invalid - should be -v$"
	error4 						db "Error 4: couldn't open the file$"
	error5 						db "Error 5: couldn't write to the file$"
	error6 						db "Error 6: couldn't read the file$"
	error7						db "Error 7: could'nt close the file$"
	error8 						db "Error 8: CRC-16 is invalid"
	error9 						db "Error 9: something else went wrong$"
	;/errors
data ends 
 



;-----------------------------------              Code                ----------------------------------- 
code1 segment 

main:
	;stack initialization 
   	mov ax, seg stack_top 
  	mov ss, ax 
  	mov sp, offset stack_top

  	; parse input
  	call parse

  	; calculate crc16
  	call crc16

    ;end the program end exit to command prompt
    call endsection

;###################################              Parser              ################################### 


parse: 								; parse arguments, calls proper procedures

	call loadtobuffer				; load arguments from buffer
	call checknumber				; check if number of args <=3 and >=2
	call setarglen 					; fill the argslength[] tab
	call setargoffsets 				; fill the argoffset[] tab
	call buffertoargs 				; write from buffer to args
ret

loadtobuffer:						; load from PSP buffer to var buffer in loop

	push ax
	push bx
	push cx
	push dx

	mov ax, seg data 				; load data segment
	mov es, ax 						; using extra segment for data

	xor cx, cx 						; set cx to 0 for loop
	mov cl, byte ptr ds:[080h]		; set cl to length of arguments
	cmp cl, 0d						
	JNE loadargs					; if no arguments do error0out

	error0out:						
	mov dx, offset error0
	call errorhandler


	loadargs:						; if there are arguments
	; input length of args
	xor bx,bx
	mov bx, offset bufferlen
	mov byte ptr es:[bx], cl

	mov si, 082h 					; move PSP offset to SI (source index)
	mov di, offset buffer 			; move buffer offset to DI (destination index)
 	
 	xor dh, dh 						; set dh to 0 for counting arg length
 	; input loop
 	mov al, byte ptr ds:[si]
 	cmp al, 21h
 	
	input:		
		call eatwhitespaces		
		mov al, byte ptr ds:[si] 	; read byte from source index to al
		mov byte ptr es:[di], al	; write byte from al to destination index
		inc dh
		inc si 						; si+=1 -> now si is pointing to next byte
		inc di 						; di+=1 -> now di is pointing to next byte
	loop input 						; cx-=1, jump to input

	mov byte ptr es:[arglen], dh	; set arglen to dh (number of characters written to buffer)
									; TEST : sets arglen to right value (ok)

	dec di 							; set last arg char to NULL (for next iteration)
	mov byte ptr es:[di], 0

								
 	pop dx
 	pop cx
 	pop bx
 	pop ax 			
ret
 	
eatwhitespaces:						; eat white spaces, insert separator after each argument

	mov al, byte ptr ds:[si]		; load next byte to al 

	cmp al, 09h						; check if space
	JNE checktab

 	skipwhitespaces:				; skipping whitespaces 
	inc si 							; si+=1 
	dec cl 							; loop counter-=1
	mov byte ptr es:[flag], 1		; set flag to high
	JMP eatwhitespaces
	
	checktab:
	cmp al, 20h						; check if tab
	JNE separator

	skipwhitespaces2:				; skipping whitespaces 
	inc si 							; si+=1 
	dec cl 							; loop counter-=1
	mov byte ptr es:[flag], 1		; set flag to high
	JMP eatwhitespaces

 	separator:
	mov ah, es:[flag]				; check if insert separator
	cmp ah, 1						; if any whitespaces were skipped
	JNE noinsert

	insert:							; insert separator to argstring, also counts arguments
	cmp di, offset buffer			; if insert to first buffer character
	JE noinsert

	;########### inserting NULL 	;
	mov byte ptr es:[di], 0		    ; set next character to NULL
	inc es:[argctr]					; increment argument counter
	inc di 							; set di to next character
 	inc dh							; in dh we store length of parsed arguments
	;########### /inserting NULL
	
	noinsert:						;	
	xor ah,ah 						; set ah to 0
	mov byte ptr es:[flag], 0  		; flag=0	
ret

checknumber:						; check number of aruments

	push ax
	mov ax, seg data
	mov ds, ax
	cmp ds:[argctr], 2				; by default argctr==0 and it is incremented when inserting separator, thus argctr==arg number-1
	JLE rightnumber

	error2out:
	mov dx, offset error2
	call errorhandler

	rightnumber:					; if number of args <= 3
	cmp ds:[argctr], 1 				; check if number of args<2
	JL error2out 					; if argnumber==1

	pop ax
ret

setarglen:							; fills argslength[] tab
	
	push ax
	push cx

	mov ax, seg data
	mov ds, ax

	xor ax, ax 						; ax=0
	xor si, si
	mov si, offset buffer 			; set si (source index) to first character of parsed arguments



	xor di, di 						; set di (destination index) to 0
	xor cx, cx 						; cx=0
	mov cl, byte ptr ds:[arglen]	; now cl = length of parsed arguments

	stlen: 							; loop for writing to argslength[] tab
		mov al, byte ptr ds:[si]	; load next character to al
		cmp al, 0 					; check if al==NULL
		JE isnull

		inc ds:[argslength+di] 		; current arg length ++
		JMP notnull

		isnull:						; if current char is null
		inc di 						; edit next (argslength[di]) length

		notnull:					; current character is not null
		inc si 						; si+=1, point to next char
	loop stlen

	pop cx
	pop ax
ret

setargoffsets: 						; fills argoffset[] tab

	push ax
	push bx
	push cx
	push si
	push di

	mov ax, seg data 				; load data segment
	mov ds, ax 


	mov si, offset buffer 			; si is pointing to first char of buffer
	mov di, 1 						; di is pointing to second element of argoffset[] tab, because first is 0 by default
	xor cx, cx 						; cx=0
	mov cl, byte ptr ds:[arglen]  	; set cx for loop
	xor ax, ax 						; ax=0
	xor bx, bx 						; bx=0

	writeoffset:
		mov al, byte ptr ds:[si] 	; mov to al next char from already parsed buffer
		cmp al, 0 					; if al == 0 (separator)
		JNE notwrite 				

		write: 						; if current char is NULL
		inc si 						; si+=1
		mov bx, si 					; bx=si, now in bx is stored offset of first character of next argument
		mov ds:[argoffset+di], bl 	; since number of chars passed in PSP <=128, bh==0
		inc di 						; di+=1, now is pointing to next element of argoffset[] tab

		notwrite:
		inc si 						; si+=1
	loop writeoffset

	pop di
	pop si
	pop cx
	pop bx
	pop ax
ret

buffertoargs: 					 	; write from buffer to arguments

	push ax
	push bx
	push dx

	mov ax, seg data
	mov ds, ax

	xor bx, bx						; bx=0 
	mov bx, offset argument1		; bx is pointing to argument1
	mov al, byte ptr ds:[argslength]; store in al length of arg1
	xor si, si 						; in si store offset of argument #1 in buffer
	call toarg 						; call writing procedure

	xor bx, bx 						; bx=0
	mov bx, offset argument2 		; bx is pointing to argument2
	xor ax, ax
	mov al, byte ptr ds:[argoffset+1]; write to al offset of argument2 in buffer
	mov si, ax 						; in si store offset of argument #2
	mov al, byte ptr ds:[argslength+1]; write to al length of argument #2
	call toarg 						;call writing procedure

	mov al, byte ptr ds:[argctr]
	cmp al, 2 						; if number of arguments is 2
	JNE nomoreargs

	xor bx, bx 						; bx=0
	mov bx, offset argument3 		; bx is pointing to argument3
	xor ax, ax
	mov al, byte ptr ds:[argoffset+2]; write to al offset of argument3 in buffer
	mov si, ax						; in si store offset of argument #3
	mov al, byte ptr ds:[argslength+2]; write to al length of argument #3
	call toarg 						; call writing procedure

	nomoreargs:

	pop dx
	pop bx
	pop ax
ret

toarg: 								; bx - offset to argument#nr, al - length of argument, si - number of chars before argument in buffer
	
	push cx 						
	xor cx, cx 					 	; cx=0
	mov cl, al  					; set counter to length of argument
	xor al, al

	writetoarg:						; write from buffer to argument#nr
		mov al, byte ptr ds:[buffer+si]
		mov byte ptr ds:[bx], al
		inc si 						; now si is pointing to next char from buffer
		inc bx 						; now bx is pointing to next char in argument#nr
	loop writetoarg
	pop cx
ret

;###################################          CRC16 section           ################################### 

crc16:

	call versionandopen	
	call calculatecrc16
	call close
ret

versionandopen: 					; checks version and tries to open the files, in case of error prints it and quits

	push ax
	push bx
	push cx
	push dx

	mov ax, seg data
	mov ds, ax

	xor ax, ax
	xor bx, bx
	mov al, byte ptr ds:[argctr]
	cmp al, 2 						; if there are 3 arguments
	JNE nomode 						; if there are 2 arguments, mode is 0
	mov byte ptr ds:[version], 1 	; else version=1 (-v)

	mov al, byte ptr ds:[argslength]; check if argument1 length equals 2
	cmp al, 2
	JNE error3out

	mov al, byte ptr ds:[argument1] ; check if first argument is valid version
	cmp al, '-' 
	JNE error3out
	mov al, byte ptr ds:[argument1+1]
	cmp al, 'v'
	JE parametervalid 				; if argument1 is a valid parameter (-v)

	error3out: 						; if not, print error3 and quit
	mov dx, offset error3
	call errorhandler

	parametervalid: 				; if first argument is a valid parameter, try to open 2 files
	xor ax, ax
	xor bx, bx
	xor cx, cx
	mov cl, byte ptr ds:[argslength+1]

	totmpfilename1:					; copy first filename to tmpfilname for file handling
		mov al, byte ptr ds:[argument2+bx]
		mov byte ptr ds:[tmpfilename+bx], al
		inc bx
	loop totmpfilename1
	mov byte ptr ds:[tmpfilename+bx], 0; last filename byte must be null for opening
	call openread 					; open file for reading
	mov ds:[handle1], ax 			; copy first file handle to handle1

	xor ax, ax
	xor bx, bx
	xor cx, cx
	mov cl, byte ptr ds:[argslength+2]

	totmpfilename2: 				; copy second file to tmpfilename for file handling
		mov al, byte ptr ds:[argument3+bx]
		mov byte ptr ds:[tmpfilename+bx], al
		inc bx
	loop totmpfilename2
	mov byte ptr ds:[tmpfilename+bx], 0; last filename byte must be null for opening
	call openread 					; open file for reading
	mov ds:[handle2], ax 			; copy second file handle to handle2
	JMP loaded

	nomode: 						; if program runs without version
	xor ax, ax
	xor bx, bx
	xor cx, cx
	mov cl, byte ptr ds:[argslength]

	totmpfilename3: 				; copy first filename to tmpfilname for file handling
		mov al, byte ptr ds:[argument1+bx]
		mov byte ptr ds:[tmpfilename+bx], al
		inc bx
	loop totmpfilename3
	mov byte ptr ds:[tmpfilename+bx], 0; last filename byte must be null for opening
	call openread  					; open file for reading
	mov ds:[handle1], ax			; copy first file handle to handle1

	xor ax, ax
	xor bx, bx
	xor cx, cx
	mov cl, byte ptr ds:[argslength+1]

	totmpfilename4:					; copy second filename to tmpfilname for file handling
		mov al, byte ptr ds:[argument2+bx]
		mov byte ptr ds:[tmpfilename+bx], al
		inc bx
	loop totmpfilename4
	mov byte ptr ds:[tmpfilename+bx], 0; last filename byte must be null for opening
	call openwrite					; open file for writing
	mov ds:[handle2], ax			; copy second file handle to handle2


	loaded:							; if files are opened, return
	pop dx
	pop cx
	pop bx
	pop ax
ret

openread: 							; opens file for reading

	push dx
	xor ax, ax
	xor dx, dx
	mov dx, offset tmpfilename 		; in tmpfilename[] is stored name of file ended with null
	mov al, 2d 						; option for int 21h (3dh) interruption (read+write)
	mov ah, 3dh 
	int 21h
	JC error4out 					; if any error with opening
	JMP filereadopened 				; if file opened

	error4out:
	mov dx, offset error4
	call errorhandler

	filereadopened:
	pop dx
ret

openwrite: 							; opens file for writing (creates new file)
	
	push dx
	push cx

	xor ax, ax
	xor dx, dx
	mov dx, offset tmpfilename  	; in tmpfilename[] is stored name of file ended with null
	mov al, 0d 						; option for int21h (3dh) interruption (write)
	mov ah, 3ch
	int 21h
	JC error4out2 					; if any error with opening
	JMP filewriteopened 			; if file opened

	error4out2:
	mov dx, offset error4
	call errorhandler

	filewriteopened:
	pop cx
	pop dx
ret

close: 								; close both files

	push ax
	push bx

	mov ax, seg data
	mov ds, ax

	mov ah, 3eh 					; close using file handles in bx and int 21h (3eh) interruption
	mov bx, ds:[handle1]
	int 21h
	JC error7out

	mov ah, 3eh						; close using file handles in bx and int 21h (3eh) interruption
	mov bx, ds:[handle2]
	int 21h
	JC error7out
	JMP closed

	error7out: 						; if any problem with closing files
	mov dx, offset error7
	call errorhandler

	closed:
	pop bx
	pop ax
ret

calculatecrc16: 					; calculates the crc16 and depending on version either saves or verifies

	push ax
	push bx
	push cx
	push dx

	mov ax, seg data 				; load data segment
	mov ds, ax

	xor ax, ax
	mov al, ds:[version] 			; check for version (1- -v)
	cmp al, 1
	JE doubleinput 					; if version is -v (2 input files)

	mov bx, ds:[handle1] 			
	call crc16calc 					; calculate crc16 for first file
	mov bx, ds:[handle2] 			
	mov ah, byte ptr ds:[bytehigh] 	
	mov al, byte ptr ds:[bytelow]
	call writecrc16 				; save crc16 to second file 
	JMP calculationfinished 	

	doubleinput: 					; if there are 2 input files
	mov bx, ds:[handle1] 			
	call crc16calc 					; calculate crc16 for first file
	mov dh, byte ptr ds:[bytehigh]
	mov dl, byte ptr ds:[bytelow]
	mov bx, ds:[handle2]
	call readcrc16 					; read crc16 from second file

	cmp ax, dx 						; compare crc16s
	JNE crc16diff 				

	mov dx, offset message1 		; if they are equal, print message1
	call print
	JMP calculationfinished

	crc16diff: 						; if they are different
	mov dx, offset message2
	call print

	calculationfinished:
	pop dx
	pop cx
	pop bx
	pop ax
ret

crc16calc: 							; calculate crc from file (handle in BX as input)

	push ax

	xor ax, ax
	mov ds:[bytehigh], 0 			; starting value for lookup table method is high=0, low=0
	mov ds:[bytelow], 0
	mov ds:[endofreading], 0 		; start reading

	calc:
		call getchar 				; (try to) get next char from file
		cmp ds:[endofreading], 1 	; if reading is finished (EOF)
		je calcfinished 			; end calculations
		call modifycrc 				; else modify crc according to loaded char
	jmp calc

	calcfinished:
	pop ax
ret

getchar: 							; get char from filebuffer or try to load IN: BX- handle, OUT: AL - char
	push bx
	push cx
	push dx

	mov dx, ds:[fbufferlength]		; check number of characters stored in buffer
	cmp dx, 0
	JG returnchar 					; if more than 0, return character

	call loadtofilebuffer 			; else try to load to buffer

	returnchar:
	cmp cl, 1 						; if loading failed
	JE endoffile 				

	filebuffernotempty:
	xor ax, ax
	xor bx, bx
	mov bx, ds:[fbufferposition] 	
	mov al, byte ptr ds:[filebuffer+bx]; read next byte/char
	inc bx
	mov ds:[fbufferposition], bx 	; move fbufferposition to next byte
	mov bx, ds:[fbufferlength] 		
	dec bx
	mov ds:[fbufferlength], bx 		; number of bytes left in file buffer -=1
	JMP charreturned

	endoffile: 						; if EOF
	mov dx, ds:[fbufferlength] 		; check if nothing is left in buffer
	cmp dx, 0 					
	JNE filebuffernotempty 			; if there is something left
	mov byte ptr ds:[endofreading], 1d; write 1 to endofreading (EOF)
	JMP charreturned


	charreturned:

	pop dx
	pop cx
	pop bx
ret

loadtofilebuffer:					; tries to load to filebuffer IN: BX- file handle OUT: cl - 1 if EOF

	push ax
	push dx

	mov cx, 4096d 					; try to load 4kB to buffer
	mov ah, 3fh
	mov dx, offset filebuffer
	int 21h
	xor cx, cx
	mov ds:[fbufferposition], 0 	; set fbufferposition to 0
	cmp ax, 0 						; if no bytes were loaded
	JNE loadmore 

	mov cl, 1d 						; if no bytes were loaded, return in cl 1

	loadmore:
	mov ds:[fbufferlength], ax 		; move to fbufferlength number of bytes read

	pop ax
	pop dx
ret

modifycrc: 							; modify crc16high and crc16low according to lookup table algorithm

	push ax
	push bx
	push cx
	push dx

	xor bx, bx
	xor cx, cx
	xor dx, dx
	mov cl, byte ptr ds:[bytelow]
	xor cx, ax
	mov bx, cx
	mov dl, byte ptr ds:[crc16high+bx]; new bytehigh
	mov al, byte ptr ds:[crc16low+bx]
	mov cl, byte ptr ds:[bytehigh]
	xor ax, cx 						; new bytelow
	mov ds:[bytehigh], dl
	mov ds:[bytelow], al

	pop dx
	pop cx
	pop bx
	pop ax
ret

writecrc16: 						; write from ax to file by bx handle
	push ax
	push cx
	push dx
	push bx
	mov di, offset crcbufferend  ; fill the buffer in from the end
	mov cx,di
	
	getwritingbuffer:
	
	mov dx, 0                     ; high-order word of numerator - always 0
	mov bx, 16
	div bx    					  ; divide DX:AX by 10. AX=quotient, DX=remainder
	cmp dl, 10d
	JL number
	
	add dl, 'A' 			      ; convert remainder > 9 to an ASCII character
	sub dl, 10d
	JMP fillwritingbuffer
	
	number:
	add dl, '0'				      ; convert remainder < 10 to an ASCII character
	
	fillwritingbuffer:
	mov byte ptr ds:[di],dl       ; put it in the print buffer
	cmp ax,0                      ; Any more digits to compute?
	JE writecrcend                ; if not, end
	
	dec di                        ; put the next digit before the current one
	JMP getwritingbuffer          ; loop

	writecrcend:
	mov dx,di    ; save, starting at the last digit computed
	pop bx ;get file handle from stack
	sub cx,di
	inc cx	;number of characters is end of buffer offset minus beginning of buffer offset + 1
	mov ah,40h
	int 21h
	JC error5out
	JMP finishwriting

	error5out:
	mov dx, offset error5
	call errorhandler

	finishwriting:
	pop dx
	pop cx
	pop ax
ret

readcrc16: 							; crc16 read from file as output in ax, file handle as input in bx

	push dx
	push cx
	call getchar
	mov cl, al
	call getchar
	mov ah, cl
	call hex2bin
	mov dl, al
	call getchar
	mov cl, al
	call getchar
	mov ah, cl
	call hex2bin
	mov ah, dl
	cmp ds:[fbufferlength], 0d
	JNE error8out
	JMP finishreading

	error8out:
	mov dx, offset error8
	call errorhandler

	finishreading:
	pop cx
	pop dx
ret

hex2bin: 							; converts 2 hexdec from ah and al into bin in al, ah as error flag

	push bx
	push cx
	push dx

	xor bx, bx

	call h2b
	cmp dl, 1
	JE error9out
	add bl, ah

	push cx 
	mov cl, 4
	shl bl, cl 						; shl bl, 4 doesnt work without .286
	pop cx

	mov ah, al
	call h2b
	cmp dl, 1
	JE error9out
	add bl, ah
	mov al, bl
	mov ah, 0
	JMP hex2binend

	error9out:
	mov dx, offset error9
	call errorhandler

	hex2binend:
	pop dx
	pop cx
	pop bx
ret

h2b:								; converts 1 hexdec to bin

	mov dl, 0 						; no error at start
	
	cmp ah, '0'
	JB h2berror 					; can't be less than 0
	 
	cmp ah, '9'
	JNA h2bdigit					; it is a digit
	
	cmp ah, 'f'
	JA h2berror 					; can't be above 'f'
	
	cmp ah, 'a'
	JNB h2bsmall					;it's a-f (small letter)
	
	cmp ah, 'F'
	JA h2berror 					;can't be above 'F'
	
	cmp ah, 'A'
	JB h2berror 					;it's less than 'F' but not a letter
	
	sub ah, 'A'
	add ah, 10 						;A is 11
	jmp h2bend

	h2bdigit:
		sub ah, '0'
		jmp h2bend
	h2bsmall:
		sub ah,'a' ;a is also 11
		add ah,10
		jmp h2bend
		
	h2berror:
		mov dl,1
		
	h2bend:		
ret

;###################################          Error handling          ################################### 

errorhandler: 						; args - error offset in dx

	call print
	call endsection
ret


;###################################          Print and exit          ################################### 
print: 								;print variable from data segment
	mov ax, seg data 				
    mov ds, ax 						; move data segment adress to ds 
    mov ah, 9h 						; interruption argument 9h (write)
    int 21h 						
ret 

endsection:							; end 
	mov ah, 04ch 					; iterruption argument 4ch (quit)
    int 21h 
ret
 
code1 ends			


;-----------------------------------              Stack               ----------------------------------- 
stos1 segment stack 
    dw  200 dup(?) 
    stack_top dw  ? 
stos1 ends 
 
end main 